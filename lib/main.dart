// Plugins
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:login/pages/customer/customer.dart';
import 'package:login/pages/home.dart';
import 'package:login/pages/product/product.dart';
import 'package:login/pages/profile/profile.dart';
import 'package:login/pages/users/users.dart';
import 'package:login/services/api.dart';
import 'package:shared_preferences/shared_preferences.dart';

// Runner
void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Login(),
  ));
}

// Login
class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

enum LoginStatus{
  notSignIn,
  signIn,
  signInCustomer
}

class _LoginState extends State<Login> {
  //Variable
  LoginStatus _loginStatus = LoginStatus.notSignIn;
  String username, password;
  final _key = new GlobalKey<FormState>();
  bool _secureText = true;

  // Set show & hide password
  showHide(){
    setState(() {
      _secureText = !_secureText;
    });
  }

  var _autovalidate = true;

  // Validation Input
  check(){
    final form = _key.currentState;
    if(form.validate()){
      form.save();
      login();
    }else{
      setState(() {
        _autovalidate = true; 
      });
    }
  }

  // Check Login
  login() async{
    final response = await http.post(BaseUrl.auth, body: {
      "username" : username,
      "password" : password
    });

    final data = jsonDecode(response.body);

    final val           = data['data'];
    int status          = data['status'];
    String pesan        = data['message'];
    int idAPI           = val['id'];
    String codeAPI      = val['code'];
    String nameAPI      = val['name'];
    String usernameAPI  = val['username'];
    int roleAPI         = val['id_role'];

    if(status==200){
      if (roleAPI.toString() == "1") {
        setState(() {
          _loginStatus = LoginStatus.signIn;
          savePref(idAPI, status, codeAPI, nameAPI, usernameAPI, roleAPI);
        });
      } else {
        setState(() {
          _loginStatus = LoginStatus.signInCustomer;
          savePref(idAPI, status, codeAPI, nameAPI, usernameAPI, roleAPI);
        });
      }
      
      print(pesan);
    }else{
      print(pesan);
    }
  }

  savePref(int id, int status, String code, String name, String username, int idrole) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("status", status);
      preferences.setInt("id", id);
      preferences.setString("code", code);
      preferences.setString("name", name);
      preferences.setString("username", username);
      preferences.setInt("id_role", idrole);
      preferences.commit();
    });
  }

  var status, role;
  getPref() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      status  =  preferences.getInt("status");
      role    =  preferences.getInt("id_role");

      _loginStatus = role.toString() == "1" 
      ? LoginStatus.signIn 
      : role.toString() == "2" ? LoginStatus.signInCustomer : LoginStatus.notSignIn;
    });
  }

  signOut() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt("status", null);
      preferences.setInt("id", null);
      preferences.setString("code", null);
      preferences.setString("name", null);
      preferences.setString("username", null);
      preferences.setInt("id_role", null);
      preferences.commit(); 

      _loginStatus = LoginStatus.notSignIn;
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    switch (_loginStatus) {
      case LoginStatus.notSignIn:
        return Scaffold(
          body: Form(
            autovalidate: _autovalidate,
            key: _key,
            child: ListView(
              padding: EdgeInsets.all(20.0),
              children: <Widget>[
              //Text Input Username 
                TextFormField(
                  validator: (e) {
                    if(!e.contains("@")){
                      return "Wrong format email for username";
                    }else{
                      return null;
                    }
                  },
                  onSaved: (e)=>username = e,
                  decoration: InputDecoration(
                    labelText: "Username"
                  ),
                ),
              //Text Input Password
                TextFormField(
                  validator: (e) {
                    if(e.isEmpty){
                      return "Please insert your password";
                    }
                  },
                  obscureText: _secureText,
                  onSaved: (e)=>password = e,
                  decoration: InputDecoration(
                    labelText: "Password",
                    suffixIcon: IconButton(
                      onPressed: showHide,
                      icon: Icon(_secureText ? Icons.visibility_off : Icons.visibility),
                    )
                  ),
                ),
              //Button Login
                MaterialButton(
                  onPressed: (){
                    check();
                  },
                  child: Text('Login'),
                ),
              // Button Register
                InkWell(
                  onTap: (){
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context)=>Register()
                    ));
                  },
                  child: Text("Create a new account, in here", textAlign: TextAlign.center),
                )

              ],
            ),
          )
        );
        break;
      case LoginStatus.signIn:
        return MainMenu(signOut);
        break;
      case LoginStatus.signInCustomer:
        return Customer(signOut);
        break;
    }
    
  }
}

// Register
class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  // Variable
  String name, username, password;
  final _key = new GlobalKey<FormState>();
  bool _secureText = true;

  // Set show & hide password
  showHide(){
    setState(() {
      _secureText = !_secureText;
    });
  }

  var validate = true;

  // Validation Input
  check(){
    final form = _key.currentState;
    if(form.validate()){
      form.save();
      save();
    }else{
      setState(() {
        validate = true;
      });
    }
  }

  save() async{
    final response = await http.post(BaseUrl.register, body: {
      "name"      : name,
      "username"  : username,
      "password"  : password
    });

    final data  = jsonDecode(response.body);
    int status  = data['status'];
    String pesan   = data['message'];
    if(status==200){
      print(pesan);
      setState(() {
        Navigator.pop(context);
      });
    }else{
      print(pesan);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Form(
        autovalidate: validate,
        key: _key,
        child: ListView(
          padding: EdgeInsets.all(20.0),
          children: <Widget>[
                //Text Input Full Name 
                  TextFormField(
                    validator: (e) {
                      if(e.isEmpty){
                        return "Please insert your full name";
                      }
                    },
                    onSaved: (e)=>name = e,
                    decoration: InputDecoration(
                      labelText: "Full Name"
                    ),
                  ),
                //Text Input Username 
                  TextFormField(
                    validator: (e) {
                      if(!e.contains("@")){
                        return "Wrong format email for username";
                      }else{
                        return null;
                      }
                    },
                    onSaved: (e)=>username = e,
                    decoration: InputDecoration(
                      labelText: "Username"
                    ),
                  ),
                //Text Input Password
                  TextFormField(
                    validator: (e) {
                      if(e.length < 8){
                        return "A minimum password of 8 characters";
                      }else{
                        return null;
                      }
                    },
                    obscureText: _secureText,
                    onSaved: (e)=>password = e,
                    decoration: InputDecoration(
                      labelText: "Password",
                      suffixIcon: IconButton(
                        onPressed: showHide,
                        icon: Icon(_secureText ? Icons.visibility_off : Icons.visibility),
                      )
                    ),
                  ),
                  MaterialButton(
                    onPressed: (){
                      check();
                    },
                    child: Text("Register"),
                  )
          ],
        ),
      ),
    );
  }
}

// Main Menu
class MainMenu extends StatefulWidget {
  // Variable
  final VoidCallback signOut;
  MainMenu(this.signOut);

  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {

  signOut(){
    setState(() {
      widget.signOut();
    });
  }

  String name="", username="";
  getPref() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      name      = preferences.getString("name");
      username  = preferences.getString("username");
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            IconButton(
              onPressed: (){
                signOut();
              },
              icon: Icon(Icons.lock_outline),
            )
          ],
        ),
        body: TabBarView(
          children: <Widget>[
            Home(),
            Product(),
            Users(),
            Profile(),
          ],
        ),
        bottomNavigationBar: TabBar(
          labelColor: Colors.blue,
          unselectedLabelColor: Colors.grey,
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(
              style: BorderStyle.none
            )
          ),
          tabs: <Widget>[
            Tab(
              icon: Icon(Icons.home),
              text: "Home",
            ),
            Tab(
              icon: Icon(Icons.apps),
              text: "Product",
            ),
            Tab(
              icon: Icon(Icons.group),
              text: "Users",
            ),
            Tab(
              icon: Icon(Icons.account_circle),
              text: "Profile",
            ),
          ],
        ),
      ),
    );
  }
}