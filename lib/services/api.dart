class BaseUrl{
    static String domain      = "http://10.1.70.78:8000";
    static String auth        = domain+"/auth";
    static String register    = domain+"/user/add";
    static String product     = domain+"/product";
    static String productAdd  = domain+"/product/add";
    static String productEdit  = domain+"/product/edit";
    static String productDelete  = domain+"/product/delete";
    static String productImage  = domain+"/assets/product/";
    static String cart  = domain+"/cart";
    static String cartAdd  = domain+"/cart/add";
    static String cartEdit  = domain+"/cart/edit";
    static String cartDelete  = domain+"/cart/delete";
    static String cartTotal  = domain+"/cart/jmlcart/";
}