import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:login/models/productModel.dart';
import 'package:login/services/api.dart';

class EditProduct extends StatefulWidget {
  // Variable
  final ProductModel model;
  final VoidCallback reload;
  EditProduct(this.model, this.reload);

  @override
  _EditProductState createState() => _EditProductState();
}

class _EditProductState extends State<EditProduct> {
  // Variable
  final _key = new GlobalKey<FormState>();
  String productname, qty, price;

  TextEditingController txtProductName, txtQty, txtPrice;

  setup(){
    txtProductName  = TextEditingController(text: widget.model.productname);
    txtQty          = TextEditingController(text: widget.model.qty);
    txtPrice        = TextEditingController(text: widget.model.price);
  }

  // Validation Input
  check(){
    final form = _key.currentState;
    if(form.validate()){
      form.save();
      update();
    }else{

    }
  }

  update() async{
    final response = await http.post(BaseUrl.productEdit, body: {
      "id"            : widget.model.id,
      "product_name"  : productname,
      "qty"           : qty,
      "price"         : price
    });
    final data = jsonDecode(response.body);
    int status = data['status'];
    String pesan = data['message'];

    if (status == 200) {
      setState(() {
        widget.reload();
        Navigator.pop(context);
      });
    } else {
      print(pesan);
    }
  }

  @override
  void initState() {
    super.initState();
    setup();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Form(
        key: _key,
        child: ListView(
          padding: EdgeInsets.all(20.0),
          children: <Widget>[      
            // Product name
            TextFormField(
              controller: txtProductName,
              validator: (e) {
                if(e.isEmpty){
                  return "Please insert your product name";
                }
              },
              onSaved: (e)=>productname=e,
              decoration: InputDecoration(
                labelText: "Product Name",
              ),
            ),

            // Quantity
            TextFormField(
              controller: txtQty,
              validator: (e) {
                if(e.isEmpty){
                  return "Please insert qty";
                }
              },
              onSaved: (e)=>qty=e,
              decoration: InputDecoration(
                labelText: "Quantity",
              ),
            ),

            // Price
            TextFormField(
              controller: txtPrice,
              validator: (e) {
                if(e.isEmpty){
                  return "Please insert price";
                }
              },
              onSaved: (e)=>price=e,
              decoration: InputDecoration(
                labelText: "Price",
              ),
            ),

            MaterialButton(
              onPressed: (){
                check();
              },
              child: Text("Update"),
            )
          ],
        ),
      ),
    );
  }
}