import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:login/helpers/currency.dart';
import 'package:login/helpers/datePicker.dart';
import 'package:login/services/api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path/path.dart' as path;

class AddProduct extends StatefulWidget {
  // Variable
  final VoidCallback reload;
  AddProduct(this.reload);

  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  // Variable
  int idUser;
  String qty, price, productname, codeUser;
  final _key = new GlobalKey<FormState>();
  File _imageFile;

  // Get Session
  getPref() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      idUser      = preferences.getInt("id");
      codeUser    = preferences.getString("code");
    });
  }

  _chooseGallery() async{
    var image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      maxHeight: 1920.0,
      maxWidth: 1080.0
    );
    setState(() {
      _imageFile = image;
    });
  }

  _chooseCamera() async{
    var image = await ImagePicker.pickImage(
      source: ImageSource.camera,
      maxHeight: 1920.0,
      maxWidth: 1080.0
    );
    setState(() {
      _imageFile = image;
    });
  }

  // Validation Input
  check(){
    final form = _key.currentState;
    if(form.validate()){
      form.save();
      save();
    }
  }

  // Save Proccess
  save() async{
    try {
      var stream = http.ByteStream(DelegatingStream.typed(_imageFile.openRead()));
      var length = await _imageFile.length();
      var uri = Uri.parse(BaseUrl.productAdd);
      var request = http.MultipartRequest("POST",uri);
      request.fields['product_name'] = productname;
      request.fields['qty'] = qty;
      request.fields['price'] = price.replaceAll(",", "");
      request.fields['created_by'] = idUser.toString();
      request.fields['code'] = codeUser;
      request.fields['expired_at'] = "$tgl";

      request.files.add(http.MultipartFile("image", stream, length, 
      filename:path.basename(_imageFile.path)));
    
      var response = await request.send();
      if(response.statusCode > 2){
        print("Image Uploaded");
        setState(() {
          widget.reload();
          Navigator.pop(context);
        });
      }else{
        print("Image Failed");
      }
    } catch (e) {
      debugPrint("Error $e");
    }
  }

  // DatePicker
  String pilihTanggal, labelText;
  DateTime tgl = new DateTime.now();
  final TextStyle valueStyle = TextStyle(fontSize: 16.0);
  Future<Null> _selectedDate(BuildContext context) async{
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: tgl,
      firstDate: DateTime(1992),
      lastDate: DateTime(2099)
    );

    if (picked != null && picked != tgl) {
      setState(() {
        tgl = picked;
        pilihTanggal = new DateFormat.yMd().format(tgl);
      });
    } else {
    }
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    var placeholder = Container(
      width: double.infinity,
      height: 150.0,
      child: Image.asset('./public/img/placeholder.gif'),
    );
    return Scaffold(
      appBar: AppBar(),
      body: Form(
        key: _key,
        child: ListView(
          padding: EdgeInsets.all(20.0),
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 150.0,
              child: InkWell(
                onTap: (){
                  _chooseCamera();
                },
                child: _imageFile == null 
                ? placeholder 
                : Image.file(
                    _imageFile, 
                    fit: BoxFit.fill
                  ),
              ),
            ),
            // Product name
            TextFormField(
              validator: (e) {
                if(e.isEmpty){
                  return "Please insert your product name";
                }
              },
              onSaved: (e)=>productname=e,
              decoration: InputDecoration(
                labelText: "Product Name",
              ),
            ),

            // Quantity
            TextFormField(
              validator: (e) {
                if(e.isEmpty){
                  return "Please insert qty";
                }
              },
              onSaved: (e)=>qty=e,
              decoration: InputDecoration(
                labelText: "Quantity",
              ),
            ),

            // Price
            TextFormField(
              inputFormatters: [
                WhitelistingTextInputFormatter.digitsOnly,
                CurrencyFormat()
              ],
              validator: (e) {
                if(e.isEmpty){
                  return "Please insert price";
                }
              },
              onSaved: (e)=>price=e,
              decoration: InputDecoration(
                labelText: "Price",
              ),
            ),
            
            // Date Picker 
            DateDropDown(
              labelText: labelText,
              valueText: new DateFormat.yMd().format(tgl),
              valueStyle: valueStyle,
              onPressed: (){
                _selectedDate(context);
              },
            ),

            MaterialButton(
              onPressed: (){
                check();
              },
              child: Text("Save"),
            )
          ],
        ),
      ),
    );
  }
}